FROM node:8.10.0-alpine
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh && npm i -g @adonisjs/cli
EXPOSE 3000 9229
WORKDIR /home/app
CMD ./scripts/start.sh
