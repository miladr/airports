'use strict'
const csv = require('csv-parser')
const fs = require('fs')
const Review = use('App/Models/Review')
const Database = use('Database')
const moment = require('moment');

class AirportController {

    // Return all Stats
    async getAllStats ({response}) {
        const stats = await Database
                                .select('airport_name')
                                .from('reviews')
                                .groupBy('airport_name')
                                .count('airport_name as total_reviews')

        return response.send(stats)
    }

    // return an airport stats
    async getAirportsStats ({ response, params }) {
        const airportStats = await Database
                                .select('airport_name')
                                .where('airport_name', params.airport)
                                .from('reviews')
                                .avg('overall_rating as overall_rating')
                                .sum('recommended as recommended')
                                .count('airport_name as total_reviews')
        
        if(airportStats[0].airport_name === null) {
            return response.status(404).send({error:'Airport not found or it has no review!'})
        }                        
                                

        response.send(airportStats)
    }

    // return an airport reviews
    async getAirportReviews ({ response, params, request }) {
        let reviewsQ = Database.select('overall_rating', 'recommended', 'reviewed_at', 'author_country', 'content')
                                .where('airport_name', params.airport)
                                .from('reviews')
                                .orderBy('reviewed_at', 'desc');

        // Min min_overall_ratting
         if(request.input('min_overall_ratting', false)) {
            reviewsQ.where('overall_rating', '>=' , request.input('min_overall_ratting'))  
         }                       

        const reviews = await reviewsQ                                               

        return response.send(reviews)
    }

    // load data from
    getLoadData () {

        fs.createReadStream('/home/app/src/public/airport.csv')
        .pipe(csv())
        .on('data', function (data) {
          const review = new Review()

          review.airport_name = data.airport_name
          review.title = data.title  || null
          review.content = data.content || null
          review.author_country = data.author_country || null
          review.reviewed_at =  data.date ? moment(data.date, "YYYY-MM-DD").format('YYYY-MM-DD') + "  00:00:00" : null
          review.airport_staff_rating = data.airport_staff_rating || null
          review.wifi_connectivity_rating = data.wifi_connectivity_rating || null
          review.airport_shopping_rating = data.airport_shopping_rating || null
          review.food_beverages_rating = data.food_beverages_rating || null
          review.terminal_signs_rating = data.terminal_signs_rating || null
          review.terminal_seating_rating = data.terminal_seating_rating || null
          review.terminal_cleanliness_rating = data.terminal_cleanliness_rating || null
          review.queuing_rating = data.queuing_rating || null
          review.overall_rating = data.overall_rating || null
          review.recommended = data.recommended || null
          
          review.save()
        })
        
        return 'Loading started'
    }
}

module.exports = AirportController
