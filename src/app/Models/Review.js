'use strict'

const Hash = use('Hash')
const Model = use('Model')

class Review extends Model {
    static get incrementing () {
        return false
    }

    static get createdAtColumn () {
        return null
    }

    static get updatedAtColumn () {
        return null
    }

}

module.exports = Review
