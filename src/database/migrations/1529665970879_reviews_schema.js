'use strict'

const Schema = use('Schema')

class ReviewsSchema extends Schema {
  up () {
    this.create('reviews', (table) => {
      table.string('airport_name')
      table.string('title')
      table.string('author_country')
      table.text('content')
      table.integer('airport_staff_rating')
      table.integer('wifi_connectivity_rating')
      table.integer('airport_shopping_rating')
      table.integer('food_beverages_rating')
      table.integer('terminal_signs_rating')
      table.integer('terminal_seating_rating')
      table.integer('terminal_cleanliness_rating')
      table.integer('queuing_rating')
      table.integer('overall_rating')
      table.integer('recommended')
      table.timestamp('reviewed_at')
    })
  }

  down () {
    this.drop('reviews')
  }
}

module.exports = ReviewsSchema
