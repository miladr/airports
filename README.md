##  Run

    docker-compose up


##  Loading Data
Send a request to:

    http://localhost:3000/api/all/stats

## Documentation

### Get api/all/stats
### Get api/[airport]/stats
### Get api/[airport]/reviews
#### Parameters:
min_overall_ratting:
if available only reviews with overall_ratting equal or bigger than min_overall_ratting will return