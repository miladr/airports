#!/bin/sh

printf 'preparing adonis'
npm i -g @adonisjs/cli


cd src
printf 'install lucid, mysql'

adonis install @adonisjs/lucid
adonis install mysql

printf 'npm install'
npm install
printf 'migration'
adonis migration:run

adonis serve --dev
